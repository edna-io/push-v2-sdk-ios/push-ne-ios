// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "EDNAPushNE",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "EDNAPushNE",
            targets: ["EDNAPushNE"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .binaryTarget(
            name: "EDNAPushNE",
            url: "https://maven-pub.edna.ru/repository/maven-public/com/mfms/ios/push-ne/2.6.0/edna-push-ne-2.6.0.zip",
            checksum: "6272025a6c6cccad906dc3c5be2b58ebda4dedd20c546ebeeb9cbe5eacf59bef"
        ),
    ]
)
